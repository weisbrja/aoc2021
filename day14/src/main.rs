use std::collections::HashMap;
use std::fs;

type ElemPairsToCounts = HashMap<(char, char), usize>;
type ElemPairsToInserts = HashMap<(char, char), char>;
type ElemsToCounts = HashMap<char, usize>;

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    let mut lines = file.lines();

    let chars: Vec<_> = lines.next().unwrap().chars().collect();
    lines.next().unwrap();
    let mut pairs = chars
        .windows(2)
        .fold(ElemPairsToCounts::new(), |mut map, pair| {
            map.entry((pair[0], pair[1]))
                .and_modify(|count| *count += 1)
                .or_insert(1);
            map
        });

    let mut inserts = ElemPairsToInserts::new();
    for line in lines {
        let chars: Vec<_> = line.chars().collect();
        let key = (chars[0], chars[1]);
        let val = chars[6];
        inserts.insert(key, val);
    }

    let mut elems = chars.iter().fold(ElemsToCounts::new(), |mut map, &elem| {
        map.entry(elem).and_modify(|count| *count += 1).or_insert(1);
        map
    });

    // part 1
    for _ in 0..10 {
        pairs = next_polymer(&pairs, &inserts, &mut elems);
    }
    println!("{}", max_minus_min(&elems));

    // part 2
    for _ in 10..40 {
        pairs = next_polymer(&pairs, &inserts, &mut elems);
    }
    println!("{}", max_minus_min(&elems));
}

fn next_polymer(
    pairs: &ElemPairsToCounts,
    inserts: &ElemPairsToInserts,
    elems: &mut ElemsToCounts,
) -> ElemPairsToCounts {
    let mut new_pairs = ElemPairsToCounts::new();
    for (pair, &count) in pairs {
        if let Some(&insert_elem) = inserts.get(pair) {
            let pair_1 = (pair.0, insert_elem);
            let pair_2 = (insert_elem, pair.1);

            new_pairs
                .entry(pair_1)
                .and_modify(|c| *c += count)
                .or_insert(count);
            new_pairs
                .entry(pair_2)
                .and_modify(|c| *c += count)
                .or_insert(count);

            elems
                .entry(insert_elem)
                .and_modify(|c| *c += count)
                .or_insert(count);
        } else {
            new_pairs
                .entry(*pair)
                .and_modify(|c| *c += count)
                .or_insert(count);
        }
    }
    new_pairs
}

fn max_minus_min(elems: &ElemsToCounts) -> usize {
    elems.values().max().unwrap() - elems.values().min().unwrap()
}
