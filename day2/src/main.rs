use std::fs;

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    println!("{}", part_1(&file));
    println!("{}", part_2(&file));
}

fn part_1(file: &str) -> i64 {
    let mut x = 0;
    let mut depth = 0;
    for line in file.lines() {
        let mut split = line.split_whitespace();
        let instruction = split.next().unwrap();
        let num: i64 = split.next().unwrap().parse().unwrap();
        match instruction {
            "forward" => x += num,
            "up" => depth -= num,
            "down" => depth += num,
            _ => unreachable!(),
        }
    }
    x * depth
}

fn part_2(file: &str) -> i64 {
    let mut aim = 0;
    let mut x = 0;
    let mut depth = 0;
    for line in file.lines() {
        let mut split = line.split_whitespace();
        let instruction = split.next().unwrap();
        let num: i64 = split.next().unwrap().parse().unwrap();
        match instruction {
            "forward" => {
                x += num;
                depth += aim * num;
            }
            "down" => aim += num,
            "up" => aim -= num,
            _ => unreachable!(),
        }
    }
    x * depth
}
