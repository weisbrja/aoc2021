use std::fs;
use std::collections::VecDeque;

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    let first_line = file.lines().next().unwrap();
    println!("{}", simulate(&first_line, 80));
    println!("{}", simulate(&first_line, 256));
}

fn simulate(input: &str, iters: u64) -> u64 {
    let mut fish_state_counts = VecDeque::from([0u64; 9]);
    for str in input.split(",") {
        let num: usize = str.parse().unwrap();
        fish_state_counts[num] += 1;
    }
    for _ in 0..iters {
        let popped = fish_state_counts.pop_front().unwrap();
        fish_state_counts.push_back(popped);
        fish_state_counts[6] += popped;
    }
    fish_state_counts.into_iter().sum::<u64>()
}
