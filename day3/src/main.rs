use std::cmp::Ordering;
use std::fs;

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    println!("{}", part_1(&file));
    println!("{}", part_2(&file));
}

fn part_1(file: &str) -> u64 {
    let lines: Vec<Vec<bool>> = file
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| match char {
                    '0' => false,
                    '1' => true,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();
    let len = lines[0].len();
    let mut gamma_rate = String::with_capacity(len);
    let mut epsilon_rate = String::with_capacity(len);
    for i in 0..len {
        let mut count = 0;
        for line in &lines {
            if line[i] {
                count += 1;
            } else {
                count -= 1;
            }
        }
        match count.cmp(&0) {
            // most common bit is 0
            Ordering::Less => {
                gamma_rate.push('0');
                epsilon_rate.push('1');
            }
            // most common bit is 1
            Ordering::Greater => {
                gamma_rate.push('1');
                epsilon_rate.push('0');
            }
            Ordering::Equal => unreachable!(),
        }
    }
    let gamma_rate = u64::from_str_radix(&gamma_rate, 2).unwrap();
    let epsilon_rate = u64::from_str_radix(&epsilon_rate, 2).unwrap();
    gamma_rate * epsilon_rate
}

fn part_2(file: &str) -> u64 {
    let mut oxygen_list: Vec<Vec<bool>> = file
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| match char {
                    '0' => false,
                    '1' => true,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();
    let mut co2_list = oxygen_list.clone();
    let len = oxygen_list[0].len();
    for i in 0..len {
        if oxygen_list.len() == 1 {
            break;
        }
        let mut count = 0;
        for line in &oxygen_list {
            if line[i] {
                count += 1;
            } else {
                count -= 1;
            }
        }
        if count >= 0 {
            // most common bit is 1 or both are equally common
            // keep every line with bit == 1
            oxygen_list.retain(|line| line[i]);
        } else {
            // most common bit is 0
            // keep every line with bit == 0
            oxygen_list.retain(|line| !line[i]);
        }
    }
    for i in 0..len {
        if co2_list.len() == 1 {
            break;
        }
        let mut count = 0;
        for line in &co2_list {
            if line[i] {
                count += 1;
            } else {
                count -= 1;
            }
        }
        if count >= 0 {
            // least common bit is 0 or both are equally common
            // keep every line with bit == 0
            co2_list.retain(|line| !line[i]);
        } else {
            // least common bit is 1
            // keep every line with bit == 1
            co2_list.retain(|line| line[i]);
        }
    }
    let oxygen: String = oxygen_list[0]
        .iter()
        .map(|&bit| if bit { '1' } else { '0' })
        .collect();
    let co2: String = co2_list[0]
        .iter()
        .map(|&bit| if bit { '1' } else { '0' })
        .collect();
    let oxygen = u64::from_str_radix(&oxygen, 2).unwrap();
    let co2 = u64::from_str_radix(&co2, 2).unwrap();
    oxygen * co2
}
