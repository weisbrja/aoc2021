use std::{collections::HashMap, fs};

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    println!("{}", part_1(&file));
    println!("{}", part_2(&file));
}

fn part_1(file: &str) -> usize {
    let lines = file.lines();
    let mut map: HashMap<(i64, i64), u64> = HashMap::new();
    for line in lines {
        let mut split = line.split(" -> ");

        let mut start = split.next().unwrap().split(',');
        let x1: i64 = start.next().unwrap().parse().unwrap();
        let y1: i64 = start.next().unwrap().parse().unwrap();

        let mut end = split.next().unwrap().split(',');
        let x2: i64 = end.next().unwrap().parse().unwrap();
        let y2: i64 = end.next().unwrap().parse().unwrap();

        if x1 == x2 && y1 != y2 {
            // vertical line
            let min;
            let max;
            if y1 < y2 {
                min = y1;
                max = y2;
            } else {
                min = y2;
                max = y1;
            }
            for y in min..=max {
                let key = (x1, y);
                if let Some(val) = map.get_mut(&key) {
                    *val += 1;
                } else {
                    map.insert(key, 1);
                }
            }
        } else if x1 != x2 && y1 == y2 {
            // horizontal line
            let min;
            let max;
            if x1 < x2 {
                min = x1;
                max = x2;
            } else {
                min = x2;
                max = x1;
            }
            for x in min..=max {
                let key = (x, y1);
                if let Some(val) = map.get_mut(&key) {
                    *val += 1;
                } else {
                    map.insert(key, 1);
                }
            }
        }
    }
    map.values().filter(|&&num| num > 1).count()
}

fn part_2(file: &str) -> usize {
    let lines = file.lines();
    let mut map: HashMap<(i64, i64), u64> = HashMap::new();
    for line in lines {
        let mut split = line.split(" -> ");

        let mut start = split.next().unwrap().split(',');
        let x1: i64 = start.next().unwrap().parse().unwrap();
        let y1: i64 = start.next().unwrap().parse().unwrap();

        let mut end = split.next().unwrap().split(',');
        let x2: i64 = end.next().unwrap().parse().unwrap();
        let y2: i64 = end.next().unwrap().parse().unwrap();

        if x1 == x2 && y1 != y2 {
            // vertical line
            let min;
            let max;
            if y1 < y2 {
                min = y1;
                max = y2;
            } else {
                min = y2;
                max = y1;
            }
            for y in min..=max {
                let key = (x1, y);
                if let Some(val) = map.get_mut(&key) {
                    *val += 1;
                } else {
                    map.insert(key, 1);
                }
            }
        } else if x1 != x2 && y1 == y2 {
            // horizontal line
            let min;
            let max;
            if x1 < x2 {
                min = x1;
                max = x2;
            } else {
                min = x2;
                max = x1;
            }
            for x in min..=max {
                let key = (x, y1);
                if let Some(val) = map.get_mut(&key) {
                    *val += 1;
                } else {
                    map.insert(key, 1);
                }
            }
        } else {
            // diagonal line
            let (min_x, start_y);
            let (max_x, end_y);
            if x1 < x2 {
                min_x = x1;
                start_y = y1;
                max_x = x2;
                end_y = y2;
            } else {
                min_x = x2;
                start_y = y2;
                max_x = x1;
                end_y = y1;
           }

            let sign = if start_y > end_y {
                -1
            } else {
                1
            };

            for d in 0..=max_x - min_x {
                let key = (min_x + d, start_y + sign * d);
                if let Some(val) = map.get_mut(&key) {
                    *val += 1;
                } else {
                    map.insert(key, 1);
                }
            }
        }
    }

    map.values().filter(|&&num| num > 1).count()
}
