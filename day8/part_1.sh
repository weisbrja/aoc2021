#!/bin/sh
cut -d '|' -f 2 input.txt | grep -E -o '\b([a-g]{2}|[a-g]{3}\b|[a-g]{4}|[a-g]{7})\b' | wc -l
