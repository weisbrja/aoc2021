use std::{fs, ops::RangeInclusive};

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    let list: Vec<u64> = file
        .lines()
        .next()
        .unwrap()
        .split(',')
        .map(|num| num.parse().unwrap())
        .collect();
    let min = *list.iter().min().unwrap();
    let max = *list.iter().max().unwrap();
    println!("{}", calc_fuel_cost(min..=max, &list, &calc_fuel_cost_1));
    println!("{}", calc_fuel_cost(min..=max, &list, &calc_fuel_cost_2));
}

fn calc_fuel_cost_1(from: u64, list: &[u64]) -> u64 {
    list.iter()
        .map(|&pos| (from as i64 - pos as i64).abs() as u64)
        .sum()
}

fn calc_fuel_cost_2(from: u64, list: &[u64]) -> u64 {
    list.iter()
        .map(|&to| {
            let n = (from as i64 - to as i64).abs() as u64;
            n * (n + 1) / 2
        })
        .sum()
}

fn calc_fuel_cost(
    range: RangeInclusive<u64>,
    list: &[u64],
    cost_fn: &dyn Fn(u64, &[u64]) -> u64,
) -> u64 {
    // this O(n^2) time complexity could be O(n log n) with binary search
    range.map(|pos| cost_fn(pos, &list)).min().unwrap()
}
