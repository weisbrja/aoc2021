use std::collections::HashSet;
use std::fs;

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();

    let mut split = file.split("\n\n");
    let data = split.next().unwrap().lines();
    let mut coords = HashSet::new();
    for line in data {
        let mut split = line.split(',');
        let x: u64 = split.next().unwrap().parse().unwrap();
        let y: u64 = split.next().unwrap().parse().unwrap();
        coords.insert((x, y));
    }

    let folds = split.next().unwrap().lines();

    let mut part_1_done = false;
    for line in folds {
        let mut split = line.split('=');
        let fold_along_x = match split.next().unwrap() {
            "fold along x" => true,
            "fold along y" => false,
            _ => unreachable!(),
        };
        let num: u64 = split.next().unwrap().parse().unwrap();
        if fold_along_x {
            // fold every x where x > num
            let coords_to_fold: Vec<(u64, u64)> =
                coords.iter().filter(|(x, _)| x > &num).cloned().collect();
            for coord in &coords_to_fold {
                coords.remove(coord);
                let new_coord = (2 * num - coord.0, coord.1);
                coords.insert(new_coord);
            }
        } else {
            // fold every y where y > num
            let coords_to_fold: Vec<(u64, u64)> =
                coords.iter().filter(|(_, y)| y > &num).cloned().collect();
            for coord in &coords_to_fold {
                coords.remove(coord);
                let new_coord = (coord.0, 2 * num - coord.1);
                coords.insert(new_coord);
            }
        }

        // print solution to part 1
        if !part_1_done {
            println!("{}", coords.len());
            part_1_done = true;
        }
    }

    // print solution to part 2
    for y in 0..=5 {
        for x in 0..=39 {
            if coords.contains(&(x, y)) {
                print!("#");
            } else {
                print!(" ");
            }
        }
        println!();
    }
}
