use std::fs;

fn main() {
    let path = format!("{}/input.txt", env!("CARGO_MANIFEST_DIR"));
    let file = fs::read_to_string(path).unwrap();
    let nums: Vec<_> = file.lines().map(|line| line.parse().unwrap()).collect();
    println!("{}", part_1(&nums));
    println!("{}", part_2(&nums));
}

fn part_1(nums: &[u64]) -> u64 {
    let mut answer = 0u64;
    for i in 0..nums.len() - 1 {
        if nums[i + 1] > nums[i] {
            answer += 1;
        }
    }
    answer
}

fn part_2(nums: &[u64]) -> u64 {
    let mut answer = 0u64;
    for i in 0..nums.len() - 3 {
        if nums[i + 3] > nums[i] {
            answer += 1;
        }
    }
    answer
}
